<?php
  set_time_limit(0);
  $DEBUG=1;

  require_once("includes/all.inc.php");

  /***************************************************************/
  /************************ ROUTING STUFF ************************/
  /***************************************************************/

  $controller = (isset($_REQUEST["controller"]) && $_REQUEST["controller"]!=null) ? $_REQUEST["controller"] : "website";

  // Call controller
  require_once("controllers/".$controller.".controller.php");

  /***************************************************************/
  /***************************************************************/

?>