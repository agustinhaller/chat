<?php

// See: http://twig.sensiolabs.org/documentation
// Using: Twig-1.12.1
require_once('includes/lib/Twig/Autoloader.php');

class TemplateEngine_Singleton
{
  private static $twig;

  private function __construct()
  {

  }

  public static function getInstance()
  {
    if(!self::$twig instanceof self)
    {
      Twig_Autoloader::register();

      // In this folder we are going to have all the templates
      $loader = new Twig_Loader_Filesystem('views');

      self::$twig = new Twig_Environment($loader, array(
        // 'cache' => 'cache'
        // In production delete this line
        'cache' => false
      ));
    }
    return self::$twig;
  }
}

Class TemplateEngine
{
  private static $vars;

  public static function get()
  {
    self::$vars = array("SITE_NAME"=>SITE_CONFIG_NAME,
                "SITE_TITLE"=>SITE_CONFIG_TITLE,
                "SITE_IMAGE"=>SITE_CONFIG_IMAGE,
                "DOMAIN_BASE"=>SITE_CONFIG_DOMAIN,
                "DESCRIPTION"=>SITE_CONFIG_DESCRIPTION,
                "KEYWORDS"=>SITE_CONFIG_KEYWORDS,
                "BING_CODE"=>SITE_CONFIG_BING_CODE,
                "FB_APP_ID"=>SITE_CONFIG_FB_APP_ID,
                "ANALYTICS_ACCOUNT"=>SITE_CONFIG_ANALYTICS_ACCOUNT,
                "ANALYTICS_ACCOUNT_NAME"=>SITE_CONFIG_ANALYTICS_ACCOUNT_NAME,
                "STATIC_URL"=>STATIC_BASE_URL,
                "SITE_URL"=>SITE_CONFIG_DOMAIN,
                "CURRENT_YEAR"=>date("Y"));

    $template_engine_singleton = TemplateEngine_Singleton::getInstance();

    return $template_engine_singleton;
  }

  public static function addVar($name, $value)
  {
    self::$vars[$name] = $value;
  }

  public static function getVars()
  {
    return self::$vars;
  }

}

?>