<?php

if(isset($DEBUG) && $DEBUG==1)
{
  ini_set('display_errors', 1);
  ini_set('error_reporting', E_ALL);
  ini_set('error_log', 'error.log');
}

session_start();

require_once("includes/utils/ChromePhp.php");
require_once("includes/lib/analytics/ss-ga.class.php");

require_once("includes/config.inc.php");
require_once("includes/site_config.inc.php");

// Used in functions.inc.php
require_once("includes/utils/mail/PHPMailer.class.php");
require_once("includes/utils/mail/SMTP.class.php");

// End

require_once("includes/utils/functions.inc.php");
require_once("includes/utils/template_engine.inc.php");


//require_once('includes/lib/sdk/src/facebook.php');
require_once('includes/lib/adodb5/adodb.inc.php');
require_once('includes/lib/amazon_s3/S3.php');

require_once("includes/db/db.class.php");

require_once("models/App.class.php");


?>