<?php
  
// VER:
//      - http://zetcode.com/db/postgresqlphp/intro/
//      - http://www.php.net/manual/en/pgsql.examples-basic.php

  class DB
  {
    private $_Link;

    public function __construct( )
    {
      $this->_Link = &ADONewConnection(DB_ADONAME);

      // $this->_Link->PConnect(DB_CONNECTION_STRING);
      $this->_Link->Connect(DB_CONNECTION_STRING);
    }

    public function link()
    {
      return $this->_Link;
    }

    // this will be called automatically at the end of scope
    public function __destruct()
    {
      //@mysql_close( $this->_Link );
    }
  }

?>