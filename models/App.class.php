<?php

class App
{

  public static function UploadVideo($video_file, $video_type )
  {
    $msg='';

    $actual_video_name = time()."_video.".$video_type;

    $s3 = new S3(awsAccessKey, awsSecretKey, true, 's3-sa-east-1.amazonaws.com');

    $response = $s3->putObject($video_file, S3_BUCKET , $actual_video_name, S3::ACL_PUBLIC_READ, array(), array("Content-Type" => 'video/'.$video_type));
    if($response)
    {
      $msg = "S3 Upload Successful."; 
      $s3file='http://'.S3_BUCKET.'.s3.amazonaws.com/'.$actual_video_name;
    }
    else
    {
      $msg = "S3 Upload Fail.";
    }

    $ret = array("url"=>$s3file);

    return $ret;

  }

  public static function UploadAudio($audio_file, $audio_type )
  {
     $msg='';

    $actual_video_name = time()."audio.".$audio_type;


    $s3 = new S3(awsAccessKey, awsSecretKey, true, 's3-sa-east-1.amazonaws.com');

    $response = $s3->putObject($audio_file, S3_BUCKET , $actual_video_name, S3::ACL_PUBLIC_READ, array(), array("Content-Type" => 'audio/'.$audio_type));
    if($response)
    {
      $msg = "S3 Upload Successful."; 
      $s3file='http://'.S3_BUCKET.'.s3.amazonaws.com/'.$actual_video_name;
    }
    else
    {
      $msg = "S3 Upload Fail.";
    }

    $ret = array("url"=>$s3file);

    return $ret;

  }

}

?>