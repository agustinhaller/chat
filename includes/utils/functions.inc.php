<?php

/**
 * Converts all accent characters to ASCII characters.
 *
 * If there are no accent characters, then the string given is just returned.
 *
 * @param string $string Text that might have accent characters
 * @return string Filtered string with replaced "nice" characters.
 */

function remove_accents($string) {
 if (!preg_match('/[\x80-\xff]/', $string))
  return $string;
 if (seems_utf8($string)) {
  $chars = array(
  // Decompositions for Latin-1 Supplement
  chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
  chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
  chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
  chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
  chr(195).chr(137) => 'E', chr(195).chr(138) => 'E',
  chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
  chr(195).chr(141) => 'I', chr(195).chr(142) => 'I',
  chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
  chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
  chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
  chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
  chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
  chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
  chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
  chr(195).chr(161) => 'a', chr(195).chr(162) => 'a',
  chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
  chr(195).chr(165) => 'a', chr(195).chr(167) => 'c',
  chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
  chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
  chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
  chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
  chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
  chr(195).chr(179) => 'o', chr(195).chr(180) => 'o',
  chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
  chr(195).chr(182) => 'o', chr(195).chr(185) => 'u',
  chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
  chr(195).chr(188) => 'u', chr(195).chr(189) => 'y',
  chr(195).chr(191) => 'y',
  // Decompositions for Latin Extended-A
  chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
  chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
  chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
  chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
  chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
  chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
  chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
  chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
  chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
  chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
  chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
  chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
  chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
  chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
  chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
  chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
  chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
  chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
  chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
  chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
  chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
  chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
  chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
  chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
  chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
  chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
  chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
  chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
  chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
  chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
  chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
  chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
  chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
  chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
  chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
  chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
  chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
  chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
  chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
  chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
  chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
  chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
  chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
  chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
  chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
  chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
  chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
  chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
  chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
  chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
  chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
  chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
  chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
  chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
  chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
  chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
  chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
  chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
  chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
  chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
  chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
  chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
  chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
  chr(197).chr(190) => 'z', chr(197).chr(191) => 's',
  // Euro Sign
  chr(226).chr(130).chr(172) => 'E',
  // GBP (Pound) Sign
  chr(194).chr(163) => '');
  $string = strtr($string, $chars);
 } else {
  // Assume ISO-8859-1 if not UTF-8
  $chars['in'] = chr(128).chr(131).chr(138).chr(142).chr(154).chr(158)
   .chr(159).chr(162).chr(165).chr(181).chr(192).chr(193).chr(194)
   .chr(195).chr(196).chr(197).chr(199).chr(200).chr(201).chr(202)
   .chr(203).chr(204).chr(205).chr(206).chr(207).chr(209).chr(210)
   .chr(211).chr(212).chr(213).chr(214).chr(216).chr(217).chr(218)
   .chr(219).chr(220).chr(221).chr(224).chr(225).chr(226).chr(227)
   .chr(228).chr(229).chr(231).chr(232).chr(233).chr(234).chr(235)
   .chr(236).chr(237).chr(238).chr(239).chr(241).chr(242).chr(243)
   .chr(244).chr(245).chr(246).chr(248).chr(249).chr(250).chr(251)
   .chr(252).chr(253).chr(255);
  $chars['out'] = "EfSZszYcYuAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy";
  $string = strtr($string, $chars['in'], $chars['out']);
  $double_chars['in'] = array(chr(140), chr(156), chr(198), chr(208), chr(222), chr(223), chr(230), chr(240), chr(254));
  $double_chars['out'] = array('OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th');
  $string = str_replace($double_chars['in'], $double_chars['out'], $string);
 }
 return $string;
}

/**
 * Checks to see if a string is utf8 encoded.
 *
 * @author bmorel at ssi dot fr
 *
 * @param string $Str The string to be checked
 * @return bool True if $Str fits a UTF-8 model, false otherwise.
 */
function seems_utf8($Str) { # by bmorel at ssi dot fr
 $length = strlen($Str);
 for ($i = 0; $i < $length; $i++) {
  if (ord($Str[$i]) < 0x80) continue; # 0bbbbbbb
  elseif ((ord($Str[$i]) & 0xE0) == 0xC0) $n = 1; # 110bbbbb
  elseif ((ord($Str[$i]) & 0xF0) == 0xE0) $n = 2; # 1110bbbb
  elseif ((ord($Str[$i]) & 0xF8) == 0xF0) $n = 3; # 11110bbb
  elseif ((ord($Str[$i]) & 0xFC) == 0xF8) $n = 4; # 111110bb
  elseif ((ord($Str[$i]) & 0xFE) == 0xFC) $n = 5; # 1111110b
  else return false; # Does not match any model
  for ($j = 0; $j < $n; $j++) { # n bytes matching 10bbbbbb follow ?
   if ((++$i == $length) || ((ord($Str[$i]) & 0xC0) != 0x80))
   return false;
  }
 }
 return true;
}

function utf8_uri_encode($utf8_string, $length = 0) {
 $unicode = '';
 $values = array();
 $num_octets = 1;
 $unicode_length = 0;
 $string_length = strlen($utf8_string);
 for ($i = 0; $i < $string_length; $i++) {
  $value = ord($utf8_string[$i]);
  if ($value < 128) {
   if ($length && ($unicode_length >= $length))
    break;
   $unicode .= chr($value);
   $unicode_length++;
  } else {
   if (count($values) == 0) $num_octets = ($value < 224) ? 2 : 3;
   $values[] = $value;
   if ($length && ($unicode_length + ($num_octets * 3)) > $length)
    break;
   if (count( $values ) == $num_octets) {
    if ($num_octets == 3) {
     $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
     $unicode_length += 9;
    } else {
     $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
     $unicode_length += 6;
    }
    $values = array();
    $num_octets = 1;
   }
  }
 }
 return $unicode;
}

/**
 * Sanitizes title, replacing whitespace with dashes.
 *
 * Limits the output to alphanumeric characters, underscore (_) and dash (-).
 * Whitespace becomes a dash.
 *
 * @param string $title The title to be sanitized.
 * @return string The sanitized title.
 */
function slugify($title) {
 $title = strip_tags($title);
 // Preserve escaped octets.
 $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
 // Remove percent signs that are not part of an octet.
 $title = str_replace('%', '', $title);
 // Restore octets.
 $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);
 $title = remove_accents($title);
 if (seems_utf8($title)) {
  if (function_exists('mb_strtolower')) {
   $title = mb_strtolower($title, 'UTF-8');
  }
  $title = utf8_uri_encode($title, 200);
 }
 $title = strtolower($title);
 $title = preg_replace('/&.+?;/', '', $title); // kill entities
 $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
 $title = preg_replace('/\s+/', '-', $title);
 $title = preg_replace('|-+|', '-', $title);
 $title = trim($title, '-');
 return $title;
}

// Like status stuff

// function parse_signed_request($signed_request, $secret) {
//     list($encoded_sig, $payload) = explode('.', $signed_request, 2);

//     // decode the data
//     $sig = base64_url_decode($encoded_sig);
//     $data = json_decode(base64_url_decode($payload), true);

//     if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
//       error_log('Unknown algorithm. Expected HMAC-SHA256');
//       return null;
//     }
//     // check sig
//     $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
//     if ($sig !== $expected_sig) {
//       error_log('Bad Signed JSON signature!');
//       return null;
//     }

//     return $data;
//   }
  
//   function base64_url_decode($input) {
//     return base64_decode(strtr($input, '-_', '+/'));
//   }


function _trackEvent($category, $action, $label="", $value="")
{
  //create new ssga object
  $ssga = new ssga(SITE_CONFIG_ANALYTICS_ACCOUNT, DOMAIN_BASE_URL);

  // if($label!=null)
  // {
  //   if($value!=null)
  //   {
  //     $ssga->set_event($category, $action, $label, $value);
  //   }
  //   else
  //   {
  //     $ssga->set_event($category, $action, $label);
  //   }
  // }
  // else
  // {
  //   $ssga->set_event($category, $action);
  // }

  $ssga->set_event($category, $action, $label, $value);
  
  $ssga->send();
}

// Cookie stuff

function restartSession()
{
  session_destroy();
  session_start();
}

function destroyCookie($cookie_name, $domain)
{
  setcookie($cookie_name, "", time()-3600, "/", $domain);
}

function saveCookie($cookie_name, $domain, $data)
{
  $s = serialize($data);
  $s = base64_encode($s);
  setcookie($cookie_name, $s, time()+60*60*24*COOKIE_DURATION_DAYS, "/", $domain);
}


function loadCookie($cookie_name)
{
  if(isset($_COOKIE[$cookie_name]))
  {
    $s = unserialize(base64_decode($_COOKIE[$cookie_name]));
    // $_SESSION['user_id'] = $s["user_id"];
    // $_SESSION['username']= $s["username"];
    // $_SESSION['name']= $s["name"];
    // $_SESSION['twitter_account'] = $s['twitter_account'];
    return $s;
  }
  return null;
}


/* Get iamge extension */
function getExtension($str) 
{
  $i = strrpos($str,".");
  if (!$i) { return ""; }
  $l = strlen($str) - $i;
  $ext = substr($str,$i+1,$l);
  return $ext;
}

function is_valid_img($img_name)
{
  //Here you can add valid file extensions. 
  $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");

  $img_ext = getExtension($img_name);
  return in_array($img_ext, $valid_formats);
}

function is_valid_image_type($img_ext)
{
  //Here you can add valid file extensions. 
  $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");

  return in_array($img_ext, $valid_formats);
}
  
function print_str($var, $title=null)
{
  $title_a = (isset($title)) ? $title : "NO TITLE";
  $var_a = (isset($var)) ? $var : "NO VAR";
  echo("<br/>".$title_a."<br/>");
  var_dump($var_a);
  echo("<br/><br/>");
}


function setPostbackMsg($var, $var_name)
{
  // Store the variable in session
  $_SESSION['tmp_'.$var_name] = $var;
}


function getPostbackMsg($var_name)
{
  // Get the variable from session
  $ret = (isset($_SESSION['tmp_'.$var_name]) && $_SESSION['tmp_'.$var_name]!="") ? $_SESSION['tmp_'.$var_name] : null;
  
  // unset($session) it
  unset($_SESSION['tmp_'.$var_name]);

  // Return it
  return $ret;
}


function logError($log_data)
{
  $log_aux = array();
  $log_aux[] = $log_data;
  $fixed_data = array("date" => date("Y-m-d H:i:s"),
                      "filename" => $_SERVER["SCRIPT_NAME"],
                      "browser" => $_SERVER['HTTP_USER_AGENT'],
                      "session" => var_export($_SESSION,true),
                      "cookie" => var_export($_COOKIE,true));

  $data = array_merge($fixed_data,$log_aux);

  $str_log = "================ NEW LOG ENTRY ================\n";

  foreach($data as $key=>$val)
  {
   $str_log .= $key." => ".$val."\n";
  }

  $fp = fopen("error.log","a");
  fwrite($fp,$str_log);
  fclose($fp);
}


function selfURL()
{
  if(!isset($_SERVER['REQUEST_URI']))
  {
    $serverrequri = $_SERVER['PHP_SELF'];
  }
  else
  {
    $serverrequri = $_SERVER['REQUEST_URI'];
  }

  $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
  $protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s;
  $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);

  return $protocol."://".$_SERVER['SERVER_NAME'].$port.$serverrequri;
}


// function saveCookie($cookie_name, $domain)
// {
//   $s = serialize($_SESSION);
//   $s = base64_encode($s);
//   setcookie($cookie_name, $s, time()+60*60*24*COOKIE_DURATION_DAYS, "/", $domain);
// }


// function loadCookie($cookie_name)
// {
//   if(isset($_COOKIE[$cookie_name]))
//   {
//     $s = unserialize(base64_decode($_COOKIE[$cookie_name]));
//     $_SESSION['user_id'] = $s["user_id"];
//     $_SESSION['username']= $s["username"];
//     $_SESSION['name']= $s["name"];
//     $_SESSION['twitter_account'] = $s['twitter_account'];
//     return true;
//   }
//   return false;
// }


function get00Time()
{
  // Get server current time in timezone 00
  $server_timezone_aux = (float)SERVER_TIMEZONE;
  $server_timezone = $server_timezone_aux*(-1);

  $current_date_00 = date("Y-m-d H:i:00", strtotime($server_timezone." hour"));

  return $current_date_00;
}


function toServerTime($str_date)
{
  $date_aux = strtotime($str_date);

  // Get server current time in timezone 00
  $server_timezone_aux = (float)SERVER_TIMEZONE;
  $server_timezone = $server_timezone_aux*(-1);

  $date_00 = date("Y-m-d H:i:s", strtotime($server_timezone." hour", $date_aux));

  return $date_00;
}


if(!function_exists('get_called_class'))
{
  function get_called_class()
  {
    $t = debug_backtrace();
    
    $t = $t[1];
    /*var_dump($t);
    if ( isset( $t['object'] ) && $t['object'] instanceof $t['class'] ){
        return get_class( $t['object'] );
    }
    */
    if(isset($t["class"])) return $t["class"];
    
    return false;
  }
}


function getIP()
{
  $ip;
  if (getenv("HTTP_CLIENT_IP"))
  $ip = getenv("HTTP_CLIENT_IP");
  else if(getenv("HTTP_X_FORWARDED_FOR"))
  $ip = getenv("HTTP_X_FORWARDED_FOR");
  else if(getenv("REMOTE_ADDR"))
  $ip = getenv("REMOTE_ADDR");
  else
  $ip = "UNKNOWN";
  return $ip;
}


function getVisitorCountry()
{    
  $ip = getIP();

  if($ip=="127.0.0.1")
  {
    $ip = "190.135.41.53";
  }

  $key = IPINFODB_API_KEY;
  
  // Create the stream context
  $context = stream_context_create(array(
      'http' => array(
          'timeout' => 8      // Timeout in seconds
      )
  ));
  
  $f = file_get_contents("http://api.ipinfodb.com/v3/ip-country/?key=$key&ip=$ip",0,$context);
  
  if(!empty($f))
  {
    $tmp = explode(";",$f);
    $country_code = $tmp[3];
    $country_name = $tmp[4];
  }

  $_SESSION['visitor_country_code'] = $country_code;
  $_SESSION['visitor_country_name'] = $country_name;
  
  return $country_code;
}  


function getSpanishCountryName($name)
{
  $name = strtolower($name);
  if($name=="spain"){
    $name ="españa";
  }
  if($name=="brazil"){
    $name = "brasil";
  } 
  if($name=="united states"){
    $name = "estados unidos";
  }
  return ucwords($name);
}


function sendPostData($url, $data)
{
  $data_str = http_build_query($data);

  $handler = curl_init();
  curl_setopt($handler, CURLOPT_URL, $url);
  curl_setopt($handler, CURLOPT_POST,true);
  curl_setopt($handler, CURLOPT_POSTFIELDS, $data_str);
  curl_setopt($handler, CURLOPT_HEADER,0);  
  curl_setopt($handler, CURLOPT_RETURNTRANSFER  ,1);
  $response = curl_exec ($handler);
  curl_close($handler);
  
  return $response;
}


function send_mail($from_mail, $from_name, $subject, $content, $recipients)
{
  $mail = new PHPMailer();

  $mail->SetFrom($from_mail, $from_name);
  $mail->Subject = utf8_decode($subject);
  $mail->MsgHTML($content);

  foreach($recipients as $recipient)
  {
    $mail->AddAddress($recipient);
  }

  $mail->IsSMTP();
  // $mail->SMTPDebug=2;

  $mail->SMTPAuth   = SMTP_AUTH;
  $mail->Port       = EMAIL_PORT;
  $mail->Host       = EMAIL_HOST;
  $mail->Username   = EMAIL_HOST_USER;
  $mail->Password   = EMAIL_HOST_PASSWORD;

  $retBool = true;
  $ret_msg="";

  if(!$mail->Send())
  {
    $retBool = false;
  }
  
  if(!$retBool)
  {
    $ret_msg = "ERROR";
  }
  else
  {
    $ret_msg = "OK";
  }

  return $ret_msg;
}


function format_date($date, $format)
{
  return date($format,strtotime($date));
}


function connect_db()
{
  $o = new DB();
  return $o->link;
}

function disconnect_db($conn)
{
  mysql_close($conn);
}


function microtime_float()
{
  list($usec, $sec) = explode(" ", microtime());
  return ((float)$usec + (float)$sec);
}


function google_translate_curl($url,$params = array(),$is_cookie_set = false)
{
  if(!$is_cookie_set)
  {
    /* STEP 1. let's create a cookie file */
    $ckfile = tempnam ("/tmp", "CURLCOOKIE");

    /* STEP 2. visit the homepage to set the cookie properly */
    $ch = curl_init ($url);
    curl_setopt ($ch, CURLOPT_COOKIEJAR, $ckfile);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec ($ch);
  }

  $str = ''; $str_arr= array();

  foreach($params as $key => $value)
  {
    $str_arr[] = urlencode($key)."=".urlencode($value);
  }

  if(!empty($str_arr))
  {
    $str = '?'.implode('&',$str_arr);
  }

  /* STEP 3. visit cookiepage.php */

  $Url = $url.$str;

  $ch = curl_init ($Url);
  curl_setopt ($ch, CURLOPT_COOKIEFILE, $ckfile);
  curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);

  $output = curl_exec ($ch);
  return $output;
}


function translate_single($word,$from = 'en', $to = 'es')
{
  $word = urlencode($word);
  $url = "http://translate.google.com/translate_a/t?client=t&text=".$word."&sl=".$from."&hl=".$to."&tl=".$to."&ie=UTF-8&oe=UTF-8&multires=1&otf=1&pc=1&ssel=6&tsel=3&sc=1";
  $name_en = (google_translate_curl($url));
  $name_en = explode('"',$name_en);
  return  $name_en[1];
}


function translate($text, $from ='en', $to='es')
{
   /*$textArray = explode('.',$text);
  $translated = array();
  foreach($textArray as $te){
    $translated[] = translate_single($te,$from, $to);
  }
  return (implode('. ',$translated));
   */
  $text = trim($text);
  $text = str_replace('"',"'",$text);
  //prettyDebug(strlen($text),"CLEAN LENGTH");
  $text_encoded = (urlencode($text));
  //prettyDebug(strlen($text_encoded),"DIRTY LENGTH");

  if(strlen($text_encoded)>1800)
  {
    $parts = ceil(strlen($text_encoded) / 1800);
    $tmp_text = explode("+",$text_encoded);
    $sentences = array();

    for($i=0;$i<$parts;$i++)
    {
      $from = (ceil((count($tmp_text)-1) / $parts) * ($i));
      $to = (ceil((count($tmp_text)-1) / $parts) * ($i+1));

      if($to>=count($tmp_text))
      {
        $to = count($tmp_text)-1;
      }

      $tmp_part = implode("+",array_slice($tmp_text, $from, $to));
      $r = file_get_contents("http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20google.translate%20where%20q%3D%22".$tmp_part."%22%20and%20target%3D%22es%22%3B&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
      $tmp_response = json_decode($r);
      $tmp_sentences = $tmp_response->query->results->json->json[0]->json->json;
      
      if(count($tmp_sentences)>0)
      {
        $sentences = array_merge($sentences,$tmp_sentences);
      }
    }
  }
  else
  {
    $url = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20google.translate%20where%20q%3D%22".$text_encoded."%22%20and%20target%3D%22es%22%3B&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
    $r = file_get_contents($url);
    $response = json_decode($r);
    $sentences = $response->query->results->json->json[0]->json;
  }

  $new_text = "";
  
  if(count($sentences)>1)
  {
    foreach($sentences as $line)
    {
      $new_text .= $line->json[0];
    }
  }
  else
  {
    $new_text = $sentences->json[0];
  }
  
  return $new_text;
}


function print_var_name($var)
{
  foreach($GLOBALS as $var_name => $value)
  {
    if($value === $var)
    {
      return $var_name;
    }
  }

  return false;
}


function prettyDebug($var,$name=null)
{
  echo "<hr />";
  echo "<font style='font-weight:bold;color:green;'>".($name!=null?$name:print_var_name($var))."</font>";
  echo ": <pre>".var_export($var,true)."</pre>";
  ob_flush();
  flush();
  echo "<hr />";
}


function realtime_echo($str){
  ob_start();
  echo $str;
  ob_flush();
  flush();
}

?>