<?php

  $action = (isset($_REQUEST["action"]) && $_REQUEST["action"]!=null) ? $_REQUEST["action"] : "start-verification";

  switch ($action)
  {
    case 'home':
      Home();
      break;

    case 'upload-video':
      UploadVideo();
      break;

     case 'upload-audio':
      UploadAudio();
      break; 

     case 'record':
      Record();
      break;
  
    default:
      # code...
      break;
  }

  function Home()
  {
    $template_engine = TemplateEngine::get();
    $vars = TemplateEngine::getVars();
    echo $template_engine->render('website/home.html', $vars);
  }


  function Record()
  {
    $template_engine = TemplateEngine::get();
    $vars = TemplateEngine::getVars();
    echo $template_engine->render('website/video_audio.html', $vars);
  }

  function UploadVideo()
  {

    // foreach(array('video') as $type) {
    //   if (isset($_FILES["${type}-blob"])) {

    //     $video_type = $_FILES["${type}-blob"]["type"];

    //     $image_data = file_get_contents($_FILES["${type}-blob"]["tmp_name"]);
    //     $type = substr($video_type, strrpos($video_type, '/')+1);
    //     $url = App::UploadVideo($image_data, $type);

    //   }
    // }

    foreach(array('audio') as $type) {
      if (isset($_FILES["${type}-blob"])) {
       
       $video_type = $_FILES["${type}-blob"]["type"];
        
          ChromePhp::log('video_type');
          ChromePhp::log($video_type);

        $image_data = file_get_contents($_FILES["${type}-blob"]["tmp_name"]);

        $type = substr($video_type, strrpos($video_type, '/')+1);

        $url = App::UploadAudio($image_data, $type);

      }
    } 
    
    echo($url["url"]);
      
  }
  

  //  function UploadAudio()
  // {
  //   foreach(array('video', 'audio') as $type) {
  //     if (isset($_FILES["${type}-blob"])) {

  //       $data = isset($_FILES["${type}-blob"]);
     
  //       if($data!=null)
  //       {
  //           $video_type = $data["type"];
  //       }

  //       $image_data = file_get_contents($_FILES["${type}-blob"]["tmp_name"]);

  //       $confirmed = App::UploadVideo($image_data, $video_type);

       
  //     }
  //   }
  //   //show result to the user
  //   $response = array("confirmed"=>$confirmed);
  //   $json_response = json_encode($response);
    
  //   echo($json_response);
      
  //   }
  


?>
