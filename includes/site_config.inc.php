<?php

  define("SITE_CONFIG_NAME", "Chat++");
  define("SITE_CONFIG_TITLE", "Chat++");
  define("SITE_CONFIG_IMAGE", "http://".DOMAIN_BASE_URL."/static/img/logo.png");
  define("SITE_CONFIG_DOMAIN",DOMAIN_BASE_URL);
  define("SITE_CONFIG_DESCRIPTION", "Chat++");

  define("SITE_CONFIG_KEYWORDS", "");
  define("SITE_CONFIG_BING_CODE", "");
  define("SITE_CONFIG_FB_APP_ID", "");
  define("SITE_CONFIG_ANALYTICS_ACCOUNT", "UA-");
  define("SITE_CONFIG_ANALYTICS_ACCOUNT_NAME", "Chat++");

?>